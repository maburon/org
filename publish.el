
;;--- general org configuration ---

;; disable skiping by flag
(setq org-publish-use-timestamps-flag nil)

;; customization of time stamp display
;; 17/02/2019
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats '("<%d/%m/%Y>" . "<%d/%m/%Y %H:%M>"))

;; remove <> around time stamps

(add-to-list 'org-export-filter-timestamp-functions
             #'uca-website-filter-timestamp)

(defun uca-website-filter-timestamp (trans back _comm)
  "Remove <> around time-stamps."
  (pcase back
    ((or `jekyll `html)
     (replace-regexp-in-string "&[lg]t;" "" trans))
    (`latex
     (replace-regexp-in-string "[<>]" "" trans))))

(defvar uca-website-TeX-macros-path
  "~/work/org/macros.tex")

;; head and header links are absolute relatively to the base
;; of the website, so we just need to change the base from dev to prod

(setq uca-website-url-base "https://perso.isima.fr/~maburon/")

(defun uca-website-html-head (base)
  (concat " <link rel='shortcut icon' href='" base "/img/favicon.ico'/> 
<link href='" base "/css/style.css' rel='stylesheet' type='text/css'>"))

(defun uca-website-html-nav (base)
  (concat
   "<div class='nav'>
<ul>
<li><a href='" base "/index.html'>Home</a></li>
<li><a href='" base "/teaching.html'>Teaching</a></li>
<li><a href='" base "/posts.html'>Posts</a></li>
<li><a href='" base "/presentations.html'>Presentations</a></li>
<li><a href='" base "/projects/'>Projects</a></li>
</ul>
</div>"))

(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(defun get-TeX-macros-string ()
  "get TeX macro file as string
escape some problematic character"
  (replace-regexp-in-string
   "%"
   "%%" 
   (replace-regexp-in-string "\\\\" "\\\\" (get-string-from-file uca-website-TeX-macros-path))))

(defun get-TeX-macros-html ()
  "get a HTML tag containing TeX macros"
  (concat "<div style='display:none;'> $$" (get-TeX-macros-string) "$$ </div>"))

(defun get-preambule-html (base)
  "get HTML preambule"
  (concat (uca-website-html-nav base) (get-TeX-macros-html)))

(defun uca-website-create-project-configuration (suffix publishing-dir base)
  "create a configuration for uca website according to publishing-dir and base"
  `((,(concat "uca-website-html-" suffix)
     :base-directory "~/work/org/"
     :publishing-directory ,publishing-dir
     :publishing-function org-html-publish-to-html
     :recursive t
     :html-toplevel-hlevel 2
     :section-numbers nil
     :with-toc nil
     :with-author nil
     :html-head ,(uca-website-html-head base)
     :html-preamble ,(get-preambule-html base)
     :html-postamble nil)
    (,(concat "uca-website-static-" suffix)
     :base-directory "~/work/org/"
     :base-extension "png\\|css\\|pdf\\|jpg\\|jpeg\\|mp4\\|html"
     :publishing-directory ,publishing-dir
     :publishing-function org-publish-attachment
     :recursive t
     :exclude "\.git/.*")
    (,(concat "uca-website-" suffix)
     :components (,(concat "uca-website-static-" suffix) ,(concat "uca-website-html-" suffix)))))

(setq org-publish-project-alist nil)
(setq org-publish-project-alist
      (append org-publish-project-alist
      ;; ` character is very important
      ;; replace it by ' and functions inside object will not be executed
      ;; function call have to be prefixed with ,
              `(
           ;;      ,@(uca-website-create-project-configuration
           ;; "dev"
           ;; "~/work/.public_html"
           ;; "")
        ,@(uca-website-create-project-configuration
           "prod"
           "/scp:uca:~/public_html/"
           uca-website-url-base)
        ("uca-website-latex"
         :base-directory "~/work/org/posts"
         :publishing-directory "~/work/virtual-graphs/notes"
         :publishing-function org-latex-publish-to-latex
         :recursive t))))


