% Created 2019-02-01 ven. 15:19
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\input{../macros}
\author{Maxime Buron}
\date{\today}
\title{Extended Reasoning Rules Support in GLAV Integration}
\hypersetup{
 pdfauthor={Maxime Buron},
 pdftitle={Extended Reasoning Rules Support in GLAV Integration},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.50.2 (Org mode 9.2)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents

The aim of this paper is to extend the reasoning capability in GLAV Integration in the following general context where: 

\begin{itemize}
\item \(\mappings\) is a set of relational GLAV mappings on source schema \(S\) and on global schema \(G\)
\item \(\rules\) is a set of existential rules using the schema \(G\)
\end{itemize}

We want to find some minimal restrictions on \(\rules\), such that we can find \(\mappings^{\rules}\) an equivalent set of GLAV mappings on source schema \(S\) and on global schema \(G\) i.e. for each \(I\) instance of \(S\), \(U\) is an instance of \(G\) such that \(I, \mappings, \rules \models U\) if and only if \(I, \mappings^{\rules} \models U\).

\section{Examples}
\label{sec:orgc6f6053}

A series of examples to show how we can go further than mappings head saturation.

\subsection{Examples of Extensions}
\label{sec:orgf6d7f5b}

\subsubsection{Saturation on Several Mappings}
\label{sec:org63b19a5}

$$\mappings = \{ V_{1}(x) \leadsto A(x), V_{2}(x) \leadsto B(x) \}$$

$$\rules = \{ A(x) \wedge B(x) \rightarrow C(x) \}$$

Here, we need to use two mappings at the same time to fully reason with \(r\). In order to handle it, we can define \(\mappings^{\rules}\) as the set of GLAV mappings containing mappings of \(\mappings\) and the following mapping:

$$V_{1}(x) \wedge V_{2}(x) \leadsto C(x)$$

\subsubsection{Mismatch with Constant in Rule}
\label{sec:orgbfb1b49}

$$\mappings = \{m: V(x,y) \leadsto T(x,y) \}$$

$$\rules = \{ r: T(x, C_{1}) \rightarrow T(x, C_{2}) \}$$

We can not apply \(r\) on the head of \(m\) in a saturation of conjunctive query on context like in \cite{HassadLearningCommonalitiesSPARQL2017}. To solve this issue, we should handle the case, where \(y\) is \(m\) is equals to \(C_{1}\). We can do it by defining a new mappings:

$$m^{r}: V(x, C_{1}) \leadsto T(x, C_{2})$$

and by defining \(\mappings^{\rules}\) as \(\{m, m^{r}\}\).

This situation doesn't appear in \cite{BuronRewritingBasedQueryAnswering2018a}, because mapping heads and rules are build such that variables and constant are in the same position in triple.

\subsection{Examples of Limits}
\label{sec:orge0fbf48}

\subsubsection{Rule for Transitivity}
\label{sec:orgad984bf}

$$\mappings = \{ m: V(x,y) \leadsto C(x,y) \}$$

$$\rules = \{ r: C(x, y) \wedge C(y, z) \rightarrow C(x, z) \}$$

In this case, we can wrap two mappings \(m\) with rule \(r\) given the following mapping: 

$$m': V(x,y) \wedge V(y,z) \leadsto C(x,z)$$

Then, we can continue to wrap this mapping and \(m\) with \(r\) etc.

\subsubsection{Simple RDFS Entailment Rule}
\label{sec:orga758f0f}

$$\mappings = \{m_{t}: V_{t}(x,y) \leadsto T(x,y), m_{c}: V_{c}(x,y) \leadsto C(y_{1}, y_{2}) \}$$

$$\rules = \{ r: T(x, y_{1}) \wedge C(y_{1}, y_{2}) \rightarrow T(x, y_{2}) \}$$

We can view above definitions as an RDFS integration system, where \(m_{t}\) (resp.  \(m_{c}\)) populates the global schema with triples form \(\triple{x}{\type}{y}\) (resp. \(\triple{y_{1}}{\subclass}{y_{2}}\)). And the rule \(r\) can be viewed as \(\triple{x}{\type}{y_{1}} \wedge \triple{y_{1}}{\subclass}{y_{2}} \rightarrow \triple{x}{\type}{y_{2}}\). 

Wrapping together \(m_{t}\) and \(m_{c}\) using \(r\) will produce the following mapping:

$$V_{t}(x,y_{1}) \wedge V_{c}(y_{1}, y_{2}) \leadsto T(x,y_{2})$$

We can wrap this mappings with \(m_{c}\) using \(r\), by continuing we will produce mappings of the form: 

$$V_{t}(x,y_{1}) \wedge V_{c}(y_{1}, y_{2}) \dots \wedge V_{c}(y_{n-1}, y_{n}) \leadsto T(x, y_{n})$$

This case doesn't appear in the setting of \cite{BuronRewritingBasedQueryAnswering2018a}, because we demand that mapping heads contain IRIs in class position.
\section{Definitions}
\label{sec:org193e210}

We now want to formalize the definition of \emph{wrapping a mapping set with a rule}. As defined in \cite{MugnierIntroductionOntologyBasedQuery2014a}, we will use the definition piece unifier of a query with a rule. We will use this definition to find piece unifier of rule body in \(\rules\) with mapping from \(\mappings\) (considering as existential rules). We can illustrate \(u\) a piece unifier of the body of the rule \(B(\bar v_{1}, \bar v_{2}) \rightarrow H(\bar v_{2}, \bar v_{3})\) with the head of mapping \(q_{1}(\bar x) \leadsto q_{2}(\bar x)\) by:

$$q_{1}(\bar x) \leadsto q_{2}(\bar x) \stackrel{u}{\leftrightarrow} B(\bar v_{1}, \bar v_{2}) \rightarrow H(\bar v_{2}, \bar v_{3})$$
such that \(u(q_{2}(\bar x)) \subseteq u(B(\bar v_{1}, \bar v_{2}))\).

Given one rule \(r\in \rules\) and mappings \(m_{1}, m_{2}, \dots, m_{n}\) such that:

\begin{enumerate}
\item there exists for \(1 \leq i \leq n, u_{i}\) a piece unifier of the head of \(r\) with \(m_{i}\).
\item piece unifiers \(u_{1}, u_{2}, \dots u_{n}\) are compatible and \(\{u_{1}(\head{m_{1}}), \dots, u_{n}(\head{m_{n}})\}\) forms a partition of \(u_{1}(u_{2}( \dots u_{n}(\body{r}))\dots)\)
\end{enumerate}
The mapping resulting of \emph{wrapping mappings \(m_{1}, \dots, m_{n}\) together with the rule \(r\)} is defined by:

$$\bigwedge_{1 \leq i \leq n} u_{i}(\body{m_{i}}) \leadsto u_{1}(u_{2}( \dots u_{n}(\head{r}))\dots)$$


\bibliographystyle{acs-nano}
\bibliography{../../blog/source/all}
\end{document}